require "rails_helper"

RSpec.describe FizzBuzzNumber, type: :model do
  subject(:number) { described_class.new(value) }

  let(:value) { fizz_buzz_numbers.keys.sample }
  let(:fizz_buzz_numbers) {
    { 1  => "1",
      2  => "2",
      3  => "Fizz",
      4  => "4",
      5  => "Buzz",
      7  => "7",
      9  => "Fizz",
      10 => "Buzz",
      15 => 'Fizz Buzz' }
  }

  describe "#initialize" do
    it "assigns the value given" do
      expect(number.value).to eq(value)
    end
  end

  describe "#to_s" do
    it "presents its value according to the Fizz Buzz rules" do
      value_as_string = fizz_buzz_numbers[value]

      expect(number.to_s).to eq(value_as_string)
    end
  end

  describe "#==" do
    it "returns true if the value of two #{described_class.name} objects is the same" do
      number2 = described_class.new(value)

      expect(number == number2).to be true
      expect(number2 == number).to be true
    end

    it "doesn't try to be overly clever" do
      Number = Struct.new(:value)
      number2 = Number.new(value)

      expect(number == number2).to be false
      expect(number2 == number).to be false
    end
  end
end
