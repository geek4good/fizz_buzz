require "rails_helper"

RSpec.describe FizzBuzzSlice, type: :model do
  subject(:slice) { described_class.new(limit, offset) }

  let(:limit) {  5 }
  let(:offset) { 10 }

  describe "#initialize" do
    it "assigns the start index and size given" do
      expect(slice.limit).to eq(limit)
      expect(slice.offset).to eq(offset)
    end
  end

  describe "#numbers" do
    it "returns the contained FizzBuzzNumber instances" do
      number_objects = (11..15).map { |value| FizzBuzzNumber.new(value) }
      expect(slice.numbers).to eq(number_objects)
    end
  end

  describe "#values" do
    it "returns the contained values" do
      fizz_buzz_values_11_to_15 = (11..15).to_a
      expect(slice.values).to eq(fizz_buzz_values_11_to_15)
    end
  end

  describe "#to_a" do
    it "returns the contained values" do
      fizz_buzz_strings_11_to_15 = %w(11 Fizz 13 14 Fizz\ Buzz)
      expect(slice.to_a).to eq(fizz_buzz_strings_11_to_15)
    end
  end
end
