require 'rails_helper'

RSpec.describe "FizzBuzz sequence", type: :feature do
  it "displays the first 100 fizz buzz numbers by default" do
    visit "/fizz-buzz"

    expect(page).to have_http_status(:success)
    expect(page).to have_selector("header h1", :text => "Fizz Buzz (1 to 100)")
    expect(page).to have_selector("tr#fizz-buzz-number-15 td.fizz-buzz-string", :text => "Fizz Buzz")
    expect(page).to have_selector("tr#fizz-buzz-number-89 td.fizz-buzz-string", :text => "89")
  end

  context "pagination" do
    it "defaults to 100 items per page" do
      visit "/fizz-buzz?page=2"

      expect(page).to have_http_status(:success)
      expect(page).to have_selector("header h1", :text => "Fizz Buzz (101 to 200)")
      expect(page).to have_selector("tr#fizz-buzz-number-133 td.fizz-buzz-string", :text => "133")
    end

    it "allows to change the page size and remembers it" do
      visit "/fizz-buzz?per_page=10"

      expect(page).to have_http_status(:success)
      expect(page).to have_selector("header h1", :text => "Fizz Buzz (1 to 10)")

      visit "/fizz-buzz"

      expect(page).to have_http_status(:success)
      expect(page).to have_selector("header h1", :text => "Fizz Buzz (1 to 10)")
    end

    it "displays page 1 when the page requested is out of bounds" do
      [0, 1000000001].each do |page_num| 
        visit "/fizz-buzz?page=#{page_num}"

        expect(page).to have_http_status(:success)
        expect(page).to have_selector("header h1", :text => "Fizz Buzz (1 to 100)")
      end
    end

    it "uses the default page size when the page size given is invalid" do
      visit "/fizz-buzz?per_page=40"

      expect(page).to have_http_status(:success)
      expect(page).to have_selector("header h1", :text => "Fizz Buzz (1 to 100)")
    end
  end
end
