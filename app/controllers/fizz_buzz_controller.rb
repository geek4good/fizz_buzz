class FizzBuzzController < ApplicationController
  DEFAULT_PAGE_SIZE = 100
  PAGE_SIZES = [10, 25, 50, 100, 250, 500]

  before_action :store_per_page_in_session

  def index
    @slice = FizzBuzzSlice.new(limit, offset)
  end

  private

  def store_per_page_in_session
    page_size_valid? and session[:per_page] = params[:per_page].to_i
    session[:per_page] ||= DEFAULT_PAGE_SIZE
  end

  def limit
    @limit ||= session[:per_page]
  end

  def offset
    @offset ||= begin
                  default_page = 1
                  page = page_within_bounds? ? params[:page].to_i : default_page
                  (page - 1) * limit
                end
  end

  def page_size_valid?
    PAGE_SIZES.include?(params[:per_page].to_i)
  end

  def page_within_bounds?
    page = params[:page].to_i
    lower_page_bound = 1
    upper_page_bound = 100_000_000_000 / limit

    lower_page_bound <= page && page <= upper_page_bound
  end
end
