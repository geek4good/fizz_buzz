class FizzBuzzNumber
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def ==(other)
    other.kind_of?(self.class) or return false
    value == other.value
  end

  def to_s
    [].tap { |out|
      out << "Fizz" if value % 3 == 0
      out << "Buzz" if value % 5 == 0
      out << value.to_s if out.empty?
    }.join(" ")
  end

  def to_partial_path
    "fizz_buzz/number"
  end
end
