class FizzBuzzSlice
  attr_reader :limit, :offset

  def initialize(limit, offset)
    @limit = limit
    @offset = offset
  end

  def numbers
    @numbers ||= begin
                   from = offset + 1
                   to   = offset + limit
                   (from..to).map(&FizzBuzzNumber.method(:new))
                 end
  end

  def values
    @values ||= numbers.map(&:value)
  end

  def to_a
    @array ||= numbers.map(&:to_s)
  end
end
